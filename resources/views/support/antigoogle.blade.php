<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>Service Unavailable</title>

    <link rel="stylesheet" href="https://cdn.staticfile.org/semantic-ui/2.4.1/semantic.min.css">
    <style>
    body > .grid {
        height: 100%;
    }
    </style>
</head>
<body>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <div class="ui">
                <h2 class="ui header">Sorry! service unavailable in your region.</h2>
                <h3 class="ui header">该地区无法使用本服务</h3>
                <div class="ui segment basic"></div>
            </div>
        </div>
    </div>

	<script src="https://cdn.staticfile.org/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdn.staticfile.org/semantic-ui/2.4.1/semantic.min.js"></script>
</body>
</html>