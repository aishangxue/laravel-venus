@extends('venus::layouts.base')


@section('js')
@parent
<script src="https://cdn.staticfile.org/lodash.js/4.17.15/lodash.min.js"></script>
<script src="https://cdn.staticfile.org/vue/2.6.10/vue.js"></script>
<script src="https://cdn.staticfile.org/axios/0.19.0/axios.min.js"></script>
<script src="https://cdn.staticfile.org/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdn.staticfile.org/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
<script src="https://cdn.staticfile.org/jquery-jsonview/1.2.3/jquery.jsonview.min.js"></script>
<script src="https://cdn.staticfile.org/tinymce/5.0.16/tinymce.min.js"></script>
<script src="https://cdn.staticfile.org/tippy.js/3.4.1/tippy.min.js"></script>
<script src="https://cdn.staticfile.org/jsoneditor/7.0.4/jsoneditor.min.js"></script>
<script src="https://cdn.staticfile.org/simplemde/1.11.2/simplemde.min.js"></script>

<script>
    $(function(){
        $('.ui.menu .ui.dropdown.item').dropdown({
            on: 'hover',
        });
        $('.logout-button').click(function(){
            $('#logout-form').submit();
        })
    })
</script>
@endsection


@section('css')
@parent
<link rel="stylesheet" href="https://cdn.staticfile.org/toastr.js/latest/css/toastr.min.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/selectize.js/0.12.6/css/selectize.min.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/selectize.js/0.12.6/css/selectize.default.min.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/jquery-jsonview/1.2.3/jquery.jsonview.min.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/tippy.js/3.4.1/tippy.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/tippy.js/3.4.1/themes/light.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/jsoneditor/7.0.4/jsoneditor.min.css">
<link rel="stylesheet" href="http://cdn.pystarter.com/venus/0.0.6/venus.min.css">
<link rel="stylesheet" href="https://cdn.staticfile.org/simplemde/1.11.2/simplemde.min.css">
@endsection


@section('page')
<div class="ui vertical segment">
    <div class="ui container">
        <div class="ui secondary menu">
            @php
            $homeUri = route('venus.home');
            @endphp
            <a class="item @if($homeUri == request()->url())active @endif" href="{{$homeUri}}">Home</a>
            @if(Venus::navigation()->creationNavItems())
            <div class="ui dropdown item">
                <i class="plus icon"></i>New
                <i class="dropdown icon"></i>
                <div class="menu">
                    @foreach(Venus::navigation()->creationNavItems() as $item)
                    <a class="item @if($item['uri'] == request()->url())active @endif" href="{{$item['uri']}}">{{$item['title']}}</a>
                    @endforeach
                </div>
            </div>
            @endif
            @foreach(Venus::navigation()->navItems() as $item)
            <a class="item @if($item['uri'] == request()->url())active @endif" href="{{$item['uri']}}">{{$item['title']}}</a>
            @endforeach
            <div class="right item">
                @foreach(Venus::navigation()->rightNavItems() as $item)
                <a class="item @if($item['uri'] == request()->url())active @endif" href="{{$item['uri']}}">{{$item['title']}}</a>
                @endforeach
                <a href="/" class="item" target="_blank"><i class="external alternate icon"></i> 网站首页</a>
                <a class="item logout-button">退出登录</a>
                <form action="{{route('venus.logout')}}" method="POST" id="logout-form">
                    {{csrf_field()}}
                </form>
            </div>
        </div>
    </div>
</div>

<div class="ui container">
    @if(isset($breadcrumb))
    <div class="ui breadcrumb mt-3">
        <a href="{{route('venus.home')}}" class="section">Home</a>
        @foreach($breadcrumb as $item)
            <div class="divider"> / </div>
            @if(count($item) == 1)
            <div class="active section">{{$item[0]}}</div>
            @else
            <a href="{{$item[1]}}" class="section">{{$item[0]}}</a>
            @endif
        @endforeach
    </div>
    @endif
    <div class="ui segment vertical">
            @yield('content')
    </div>
</div>
@endsection
