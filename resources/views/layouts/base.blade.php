<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>{{$title ?? 'Nova Page'}}</title>

    @section('css')
	<link rel="stylesheet" href="https://cdn.staticfile.org/semantic-ui/2.4.1/semantic.min.css">
    @show

	@stack('css')

	@stack('headJs')
</head>
<body id="v">
	@section('page')
	@show

    @section('js')
	<script src="https://cdn.staticfile.org/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://cdn.staticfile.org/semantic-ui/2.4.1/semantic.min.js"></script>
    @show

	@stack('js')
</body>
</html>