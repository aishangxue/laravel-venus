<div class="ui pagination menu">
    @foreach ($elements as $element)
        @if (is_string($element))
            <span class="disabled item">{{ $element }}</span>
        @endif

        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <span class="active item">{{ $page }}</span>
                @else
                    <a href="{{ $url }}" class="item">{{ $page }}</a>
                @endif
            @endforeach
        @endif
    @endforeach
</div>