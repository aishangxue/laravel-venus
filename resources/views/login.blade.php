@extends('venus::layouts.base')

@push('css')
<style>
body > .grid {
    height: 100%;
}
.column {
    max-width: 450px;
}
</style>
@endpush

@section('page')
<div class="ui middle aligned center aligned grid">
    <div class="column">
        <h2 class="ui teal image header">
            <div class="content">
                Login
            </div>
        </h2>
        <form class="ui large form @if(isset($errors) && $errors->any())error @endif" action="{{route('venus.postLogin')}}" method="POST">
            {{csrf_field()}}
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="email" placeholder="E-mail address">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="field" style="display: none"><input type="checkbox" name="remember" checked></div>
                <button type="submit" class="ui fluid large teal submit button">Login</button>
            </div>

            <div class="ui error message">
            @if(isset($errors))
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            @endif
            </div>
        </form>
    </div>
</div>
@endsection