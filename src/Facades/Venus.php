<?php

namespace SC\Venus\Facades;

use Illuminate\Support\Facades\Facade;


class Venus extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \SC\Venus\Venus::class;
    }
}