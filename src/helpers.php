<?php

if (!function_exists('venus_stub_path')) {
    function venus_stub_path($path)
    {
        return realpath(__DIR__ . '/../resources/stubs') . '/' . $path;
    }
}


if (!function_exists('venus_app_path')) {
    function venus_app_path($path='')
    {
        $result = app_path('Venus');
        if (!empty($path)) {
            $result .= '/' . $path;
        }
        return $result;
    }
}

if (!function_exists('venus_controller_path')) {
    function venus_controller_path($path='')
    {
        $result = venus_app_path('Controllers');
        if (!empty($path)) {
            $result .= '/' . $path;
        }
        return $result;
    }
}

if (!function_exists('venus_view_path')) {
    function venus_view_path($path='') {
        $result = resource_path('views/venus');
        if (!empty($path)) {
            $result .= '/' . $path;
        }
        return $result;
    }
}