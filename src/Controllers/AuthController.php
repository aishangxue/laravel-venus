<?php

namespace SC\Venus\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/venus';

    public function __construct()
    {
        $this->middleware('venus.guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('venus::login');
    }
}
