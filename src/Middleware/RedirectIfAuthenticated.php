<?php

namespace SC\Venus\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;


class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/venus');
        }

        return $next($request);
    }
}