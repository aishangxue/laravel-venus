<?php

namespace SC\Venus\Middleware;

use Closure;
use SC\Venus\Facades\Venus;
use Illuminate\Http\Request;


class Bootstrap
{
    public function handle(Request $request, Closure $next)
    {
        Venus::bootstrap();

        return $next($request);
    }
}
