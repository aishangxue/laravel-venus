<?php

namespace SC\Venus\Console;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class InstallCommand extends Command
{
    protected $signature = 'venus:install';
    protected $description = 'Install venus';

    public function handle(FileSystem $fs)
    {
        if (!\file_exists(\venus_view_path())) {
            $fs->makeDirectory(\venus_view_path());
        }

        $files = [
            'admin.scss.stub' => \resource_path('sass/admin.scss'),
            'admin.js.stub' => \resource_path('js/admin.js'),
            'app.blade.php.stub' => \venus_view_path('app.blade.php'),
        ];
        foreach($files as $from => $to) {
            if (!\file_exists($to)) {
                $fs->copy(\venus_stub_path($from), $to);
                $this->info("create file: ${to}");
            }
        }

        $this->info('install finished!');
        $this->info("Copy following mix config:");
        $this->warn("
mix.webpackConfig({
    externals: {
        'Vue': 'Vue',
    }
}).js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/admin.js', 'public/js')
    .sass('resources/sass/admin.scss', 'public/css');");
    }
}