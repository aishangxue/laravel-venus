<?php

namespace SC\Venus\Console;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;


class ControllerCommand extends Command
{
    protected $signature = 'venus:controller {name}';
    protected $description = 'Add venus controller';

    public function handle(FileSystem $fs)
    {
        $name = $this->argument('name');
        if (!file_exists(venus_app_path())) {
            $fs->makeDirectory(venus_app_path());
        }
        if (!file_exists(venus_controller_path())) {
            $fs->makeDirectory(venus_controller_path());
            $this->info('create directory ' . venus_controller_path());
        }

        $dstPath = venus_controller_path($name . '.php');
        if (!file_exists($dstPath)) {
            $content = $fs->get(venus_stub_path('Controller.php.stub'));
            $fs->put($dstPath, $this->renderController($name, $content));
            $this->info('create file '. $dstPath);
        } else {
            $this->error('file ' . $dstPath . ' already exists');
        }
    }

    public function renderController($name, $content)
    {
        $content = str_replace('{{{name}}}', $name, $content);
        return $content;
    }
}