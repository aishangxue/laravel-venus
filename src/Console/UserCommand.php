<?php

namespace SC\Venus\Console;

use Illuminate\Support\Str;
use Illuminate\Console\Command;


class UserCommand extends Command
{
    protected $signature = 'venus:user {email} {password}';
    protected $description = 'Add admin user account';

    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');

        \App\User::create([
            'email' => $email,
            'name' => $email,
            'password' => bcrypt($password),
        ]);
        $this->info('create user success');
    }
}