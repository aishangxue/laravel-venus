<?php

namespace SC\Venus\Support\Util;


class StrUtil
{
    static public $sbc2dbc = ['０' => '0', '１' => '1', '２' => '2', '３' => '3', '４' => '4',
    '５' => '5', '６' => '6', '７' => '7', '８' => '8', '９' => '9',
    'Ａ' => 'A', 'Ｂ' => 'B', 'Ｃ' => 'C', 'Ｄ' => 'D', 'Ｅ' => 'E',
    'Ｆ' => 'F', 'Ｇ' => 'G', 'Ｈ' => 'H', 'Ｉ' => 'I', 'Ｊ' => 'J',
    'Ｋ' => 'K', 'Ｌ' => 'L', 'Ｍ' => 'M', 'Ｎ' => 'N', 'Ｏ' => 'O',
    'Ｐ' => 'P', 'Ｑ' => 'Q', 'Ｒ' => 'R', 'Ｓ' => 'S', 'Ｔ' => 'T',
    'Ｕ' => 'U', 'Ｖ' => 'V', 'Ｗ' => 'W', 'Ｘ' => 'X', 'Ｙ' => 'Y',
    'Ｚ' => 'Z', 'ａ' => 'a', 'ｂ' => 'b', 'ｃ' => 'c', 'ｄ' => 'd',
    'ｅ' => 'e', 'ｆ' => 'f', 'ｇ' => 'g', 'ｈ' => 'h', 'ｉ' => 'i',
    'ｊ' => 'j', 'ｋ' => 'k', 'ｌ' => 'l', 'ｍ' => 'm', 'ｎ' => 'n',
    'ｏ' => 'o', 'ｐ' => 'p', 'ｑ' => 'q', 'ｒ' => 'r', 'ｓ' => 's',
    'ｔ' => 't', 'ｕ' => 'u', 'ｖ' => 'v', 'ｗ' => 'w', 'ｘ' => 'x',
    'ｙ' => 'y', 'ｚ' => 'z','（' => '(', '）' => ')', '〔' => '[', '〕' => ']',
    '【' => '[','】' => ']', '〖' => '[', '〗' => ']', '“' => '[', '”' => ']',
    '‘' => '[', "'" => ']', '｛' => '{', '｝' => '}', '《' => '<','》' => '>',
    '％' => '%', '＋' => '+', '—' => '-', '－' => '-', '～' => '-','：' => ':',
    '。' => '.', '、' => ',', '，' => '.', '、' => '.', '；' => ',', '？' => '?',
    '！' => '!', '…' => '-', '‖' => '|', '”' => '"', "'" => '`', '‘' => '`',
    '｜' => '|', '〃' => '"','　' => ' '];


    /**
     * 全角转半角
     */
    static public function sbc2dbc($str)
    {
        return strtr($str, static::$sbc2dbc);
    }

    /**
     * 将 换行、制表符 替换为空格
     * 合并空格
     */
    static public function normalizeWhitespace($str)
    {
        return preg_replace("/\s+/", " ", $str);
    }

    /**
     * 将 换行、制表符 替换为空格
     * 合并空格
     */
    static public function replaceSpecialChars($str, $replacement=' ')
    {
        return preg_replace('/[[:punct:]]/i', $replacement, $str);
    }

    /**
     * 归一化搜索字符
     */
    static public function normalizeSearch($str, $maxLength=-1)
    {
        $clean = static::sbc2dbc($str);
        $clean = static::replaceSpecialChars($clean);
        $clean = trim(static::normalizeWhitespace($clean));
        if ($maxLength > 0) {
            $clean = mb_substr($clean, 0, $maxLength);
        }
        return $clean;
    }
}