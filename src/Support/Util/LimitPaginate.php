<?php

namespace SC\Venus\Support\Util;

use Illuminate\Pagination\LengthAwarePaginator;


class LimitPaginate
{
    static function fromQuery($query, $perPage=10, $maxPage=10)
    {
        $maxCount = $perPage * $maxPage;
        $page = LengthAwarePaginator::resolveCurrentPage();
        $page = min($page, 10);
        $paginate = $query->paginate($perPage);

        return new LengthAwarePaginator($paginate->getCollection(), $paginate->total() < $maxCount ? $paginate->total(): $maxCount, $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
    }
}