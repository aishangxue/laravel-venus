<?php

namespace SC\Venus\Support;


class Navigation
{
    protected $navItems = [];
    protected $creationNavItems = [];
    protected $rightNavItems = [];

    public function navItems() {
        return $this->navItems;
    }

    public function creationNavItems() {
        return $this->creationNavItems;
    }

    public function rightNavItems() {
        return $this->rightNavItems;
    }

    public function addNavItem($title, $uri, $order) {
        $this->sortInsert($this->navItems, [
            'title' => $title,
            'uri' => $uri,
            'order' => $order,
        ]);
        return $this;
    }

    public function addCreationNavItem($title, $uri, $order){
        $this->sortInsert($this->creationNavItems, [
            'title' => $title,
            'uri' => $uri,
            'order' => $order,
        ]);
        return $this;
    }

    public function addRightNavItem($title, $uri, $order){
        $this->sortInsert($this->rightNavItems, [
            'title' => $title,
            'uri' => $uri,
            'order' => $order,
        ]);
        return $this;
    }

    protected function sortInsert(&$array, $item) {
        $len = count($array);
        $index = 0;
        while($index < $len) {
            if ($array[$index]['order'] >= $item['order']) {
                break;
            }
            $index --;
        }

        array_splice($array, $index, 0, [$item]);
    }
}