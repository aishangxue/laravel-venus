<?php

namespace SC\Venus\Support\Traits;
use Illuminate\Pagination\LengthAwarePaginator;

trait LimitPaginate
{
    public function scopeLimitPaginate($query, $per_page=10, $limit=100)
    {
        $page = LengthAwarePaginator::resolveCurrentPage();
        $page = min($page, 10);
        $paginate = $query->paginate($per_page);

        return new LengthAwarePaginator($paginate->getCollection(), $paginate->total() < $limit ? $paginate->total(): $limit, $per_page, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
    }
}