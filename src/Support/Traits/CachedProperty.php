<?php

namespace SC\Venus\Support\Traits;

use Closure;

trait CachedProperty
{
    protected $cachedProperty = [];
    static protected $clsCachedProperty = [];

    protected function cached(string $key, Closure $closure)
    {
        if (!array_key_exists($key, $this->cachedProperty)) {
            $this->cachedProperty[$key] = $closure->bindTo($this)();
        }

        return $this->cachedProperty[$key];
    }

    static protected function staticCached(string $key, Closure $closure)
    {
        if (!array_key_exists($key, static::$clsCachedProperty)) {
            static::$clsCachedProperty[$key] = $closure();
        }

        return static::$clsCachedProperty[$key];
    }
}