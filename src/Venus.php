<?php

namespace SC\Venus;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Closure;

class Venus
{
    protected $controllers = null;
    protected $navigation = null;

    public function bootstrap()
    {
        return;
    }

    public function discoveryControllers()
    {
        if (is_null($this->controllers)) {
            $this->controllers = [];
            if (file_exists(venus_controller_path())) {
                if ($files = scandir(venus_controller_path())) {
                    foreach($files as $file) {
                        if (Str::endsWith($file, 'Controller.php')) {
                            $this->controllers[] = 'App\\Venus\\Controllers\\' . Str::replaceFirst('.php', '', $file);
                        }
                    }
                }
            }
        }

        return $this->controllers;
    }

    public function navigation() {
        if (is_null($this->navigation)) {
            $this->navigation = new Support\Navigation;
            foreach($this->discoveryControllers() as $controller) {
                if ($controller::navOrder() < 0) {
                    continue;
                }
                $controller::registerNavigation($this->navigation);
            }
        }

        return $this->navigation;
    }
}